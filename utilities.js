
// A simple utilities module that can be passed on across the application.
var utilities = {
    getDistance : function( latitude1, latitude2, longitude1, longitude2 ) {

        function deg2rad(deg) {
          return deg * (Math.PI/180)
        }

        var R = 6371; // Earth radius
        var latInRad1 = deg2rad(latitude1);
        var latInRad2 = deg2rad(latitude2);
        var diffLatInRad = deg2rad(latitude2-latitude1);
        var diffLongInRad = deg2rad(longitude2-longitude1);

        var a = Math.sin(diffLatInRad/2) * Math.sin(diffLatInRad/2) +
                Math.cos(latInRad1) * Math.cos(latInRad2) *
                Math.sin(diffLongInRad/2) * Math.sin(diffLongInRad/2);

        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        var d = R * c;

        return d;
    },

    sortCustomers : function( customersArray ) {
        var compareFunc = function( customer1, customer2 ) {
            return customer1.user_id > customer2.user_id ? 1 : -1
        }

        return customersArray.sort(compareFunc);
    }
}

module.exports = utilities;