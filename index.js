var userLocationData = require('./userLocationData');
var cityLocationData = require('./cityLocationData');
var calculateCustomersClose = require( './calculateCustomersClose');

// Get the data from the data.js module. Program does not care where the data is
// database / filesystem / hardcoded .. just needs access to the data

var customerData = userLocationData.getData();

// If no customer data available do not continue
if( !customerData || customerData.length == 0 ) {
    return;
}

var customerGeoData = JSON.parse(customerData);

// Get the city location information - other cities can be used
var cityLocation = cityLocationData('Dublin');

// If no city data available don't continue
if( !cityLocation ) return;

// If no customer data is available don't continue .. possibly data is corrupted
// so parse did not work
if( !customerGeoData ) return;

console.log(' --- Dublin --- ')

// All good, calculate the users who are close enough
calculateCustomersClose( cityLocation, customerGeoData );

console.log(' --- Zagreb --- ')

// Get the city location information - other cities can be used
var cityLocationZagreb = cityLocationData('Zagreb');

// Try the same thing with Zagreb, no go -- none close enough :(
calculateCustomersClose( cityLocationZagreb, customerGeoData );

console.log(' --- Zagreb with bigger range --- ')

// Try the same thing with Zagreb, now with bigger range.
calculateCustomersClose( cityLocationZagreb, customerGeoData, '10000' );
