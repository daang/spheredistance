var userLocationData = require('./userLocationData');
var cityLocationData = require('./cityLocationData');
var calculateCustomersClose = require( './calculateCustomersClose');
var utilities = require( './utilities');

// Test city location geo data available
var dublinExpect = { lat : 53.3381985 , lon: -6.2592576 };
var dublinGeo = cityLocationData('Dublin');

if( dublinGeo &&
    dublinExpect.lat === dublinGeo.latitude &&
    dublinExpect.lon === dublinGeo.longitude) {

    console.log('SUCCESS - Dublin data available');
} else {
    console.log('FAIL - Dublin data not available');
}

var zagrebExpect = { lat : 45.8167 , lon: 15.9833 };
var zagrebGeo = cityLocationData('Zagreb');

if( zagrebGeo &&
    zagrebExpect.lat === zagrebGeo.latitude &&
    zagrebExpect.lon === zagrebGeo.longitude) {

    console.log('SUCCESS - Zagreb data available');
} else {
    console.log('FAIL - Zagreb data not available');
}


// test distance method
var testUsers = [{
            "latitude": "52.986375",
            "user_id": 12,
            "name": "Christina McArdle",
            "longitude": "-6.043701"
        }, {
            "latitude": "51.92893",
            "user_id": 1,
            "name": "Alice Cahill",
            "longitude": "-10.27699"
        }
]

var distance = calculateCustomersClose( dublinGeo, testUsers );

if( distance.length == 1 ) {
    console.log('SUCCESS - correct number of close customers');
} else {
    console.log('FAIL - number of customers not correct');
}

// Reduce the range but leave the same assert as above .. it will fail, should be 0
var distance = calculateCustomersClose( dublinGeo, testUsers, 10 );

if( distance.length == 1 ) {
    console.log('SUCCESS - correct number of close customers');
} else {
    console.log('FAIL - number of customers not correct');
}

// Test utility methods

var expectDistance = 41.68;

var calculatedDistance = utilities.getDistance(
                            dublinGeo.latitude,
                            testUsers[0].latitude,
                            dublinGeo.longitude,
                            testUsers[0].longitude
);

if( parseFloat(calculatedDistance).toFixed(2) == expectDistance ) {
    console.log('SUCCESS - calculated distance is correct.')
} else {
    console.log('FAIL - calculated distance not correct.')
}


// Add stupid range, range will be set to 100 by default
var distance = calculateCustomersClose( dublinGeo, testUsers, '!ssa2432S' );

if( parseFloat(calculatedDistance).toFixed(2) == expectDistance ) {
    console.log('SUCCESS - calculated distance is correct with bad range input.')
} else {
    console.log('FAIL - calculated distance not correct.')
}
