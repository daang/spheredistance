// read the users data from the file system .. this can easily be changed to
// read from the database

var fs = require('fs');

var userLocationData = {
    getData : function() {
        var data = [];

        try {
            data = fs.readFileSync('data.json', 'utf-8');
        } catch(e) {
            console.log('Error getting data');
        }

        return data;
    }
}

module.exports = userLocationData;