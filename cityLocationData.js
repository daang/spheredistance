// Keep a separate location for city Geo data .. Encapsulate the implementation
// in a easily accessible function that can be changed according to further
// requirements - keep city geo data in the database and fetch them here for
// instance

var cityLocationData = function( city ) {

    var cityData = {};

    switch (city) {

        case 'Dublin' :

            cityData.latitude = 53.3381985;
            cityData.longitude = -6.2592576;

        break;

        case 'Zagreb' :

            cityData.latitude = 45.8167;
            cityData.longitude = 15.9833;

        break;

        case 'Olso' :

            cityData.latitude = 59.9500;
            cityData.longitude = 10.7500;

        break;

        // Add as much cities as required below ..

        default :
            cityData = null;
        break;

    }

    return cityData;
}

module.exports = cityLocationData;