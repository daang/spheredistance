var utilities = require( './utilities');

var calculateCustomersClose = function( cityGeoData, customersList, range ) {

    var customersClose = [];

    // Go through all people and see who is less than 100km
    customersList.forEach( function( customer ) {
        var distance = utilities.getDistance( cityGeoData.latitude, customer.latitude, cityGeoData.longitude, customer.longitude );

        // If no range specified default to 100 km
        range = range || 100;

        // Check if range is a number .. if not then don't calculate the distance
        if( isNaN( range ) ) {
            console.log('Range is not a number');
            return;
        }

        // Specificaly set the number base to 10 since somewhere it's octal
        if( distance < parseInt( range, 10 ) ) {
            customersClose.push(customer);
        }
    });

    // Sort them ascending by id
    customersClose = utilities.sortCustomers(customersClose);

    // Output their names to the console
    customersClose.forEach( function( customer ) {
        console.log(customer.name + ' - ' + customer.user_id)
    });

    return customersClose;

}

module.exports = calculateCustomersClose;